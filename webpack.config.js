const path = require("path");
const VueLoaderPlugin = require('vue-loader/lib/plugin');

module.exports = (x, { mode }) => {
  const DEV = mode === "development";

  return {
    entry: [
      "@babel/polyfill",
      path.resolve(__dirname, 'src/index.js'),
    ],
    devtool: DEV ? "eval" : "source-map",
    output: {
      path: path.resolve(__dirname, 'dist'),
      filename: 'main.js'
    },
    module: {
      rules: [
        {
          test: /\.js$/,
          exclude: /node_modules/,
          use: {
            loader: "babel-loader"
          }
        },
        {
          test: /\.vue$/,
          loader: 'vue-loader',
          options: {
            cssModules: {
              modules: true,
              importLoaders: 1,
              localIdentName: '[hash:base64]'
            }
          }
        },
        {
          test: /\.css$/,
          use: [
            'vue-style-loader',
            'css-loader'
          ]
        }
      ]
    },
    resolve: {
      alias: {
        'vue$': 'vue/dist/vue.esm.js'
      },
      extensions: ['*', '.js', '.vue', '.json']
    },
    plugins: [
      new VueLoaderPlugin()
    ],

    devServer: {
      port: 3000,
      compress: true,
      contentBase: path.join(__dirname, 'dist'),
    }
  }
};
