import Vue from "vue";

import { store } from "./store/store";

import AppContainer from "./containers/AppContainer";

new Vue({
  store,
  render: h => h(AppContainer)
}).$mount("#app");
