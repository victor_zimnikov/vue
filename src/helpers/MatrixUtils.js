import { flatMap } from "lodash-es";

export function replaceRows(matrix, from, to) {
  const newMatrix = [...matrix];
  const replacingRow = matrix[from];
  const replaceableRow = matrix[to];

  newMatrix[to] = replacingRow;
  newMatrix[from] = replaceableRow;

  return newMatrix;
}

export function subtractRows(matrix, row, from, factor = 1) {
  const newMatrix = [...matrix];

  newMatrix[from] = matrix[from].map((x, idx) => x - matrix[row][idx] * factor);

  return newMatrix;
}

export function countNonEmptyRows(matrix = []) {
  return matrix.filter(x => x.filter(y => y !== 0).length > 0).length;
}
