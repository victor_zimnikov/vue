import axios from "axios";

import { BASE_AIR_API_URL } from "../constants/ApiContants";

export class SearchApi {
  constructor(token) {
    this.config = {};

    this.addHeader("etm-auth-key", token);
  }

  addHeader(name, value) {
    this.config = {
      ...this.config,

      headers: {
        ...this.config.headers,

        [name]: value
      }
    };
  }

  addParam(name, value) {
    this.config = {
      ...this.config,

      params: {
        ...this.config.params,

        [name]: value
      }
    };
  }

  initSearch(data) {
    return axios.post(`${BASE_AIR_API_URL}/search`, data, this.config);
  }

  search(requestId) {
    this.addParam("request_id", requestId);

    return axios.get(`${BASE_AIR_API_URL}/offers`, this.config);
  }
}
