import { toFinite } from "lodash-es";

import { SearchApi } from "../api/SearchApi";
import { sleep } from "../helpers/SystemUtils";

function filterOffersByDurations(offers = [], filter) {
  const minDuration =
    toFinite(filter.minDuration) > 0 ? toFinite(filter.minDuration) : 0;
  const maxDuration =
    toFinite(filter.maxDuration) > 0 ? toFinite(filter.maxDuration) : 9999999;

  return offers
    .map(root => ({
      ...root,
      offers: root.offers.filter(offer => {
        const duration = offer.segments.reduce(
          (acc, item) => acc + toFinite(item.duration_minutes),
          0
        );

        return minDuration <= duration && maxDuration >= duration;
      })
    }))
    .filter(x => x.offers.length > 0);
}

async function tryGetOffers(api, requestId) {
  return api.search(requestId).then(async ({ data }) => {
    switch (data.status) {
      case "InProcess": {
        await sleep(2000);

        return tryGetOffers(api, requestId);
      }

      case "NoResults": {
        throw new Error("No Results");
      }

      default: {
        return data;
      }
    }
  });
}

export const searchState = {
  offers: [],
  filter: {},
  requestId: 0,
  directions: [],
  requesting: false,
  openedCarrier: "",
  searchResult: {}
};

export const searchMutations = {
  resetSearch: state => {
    state.search = {
      ...state.search,

      searchResult: {},
      offers: [],
      directions: []
    };
  },
  saveRequestId: (state, payload) => {
    state.search = {
      ...state.search,

      requestId: payload
    };
  },
  saveSearchResults: (state, data) => {
    state.search = {
      ...state.search,

      searchResult: data,

      offers: data.offers || [],
      directions: data.directions || []
    };
  },
  startRequesting: state => {
    state.search = {
      ...state.search,

      requesting: true
    };
  },
  stopRequesting: state => {
    state.search = {
      ...state.search,

      requesting: false
    };
  },
  openCarrier: (state, carrierCode) => {
    state.search = {
      ...state.search,

      openedCarrier: carrierCode || ""
    };
  },
  changeFilter: (state, newFilter = {}) => {
    const filter = {
      ...state.filter,
      ...newFilter
    };

    state.search = {
      ...state.search,

      filter,

      offers: filterOffersByDurations(state.search.searchResult.offers, filter)
    };
  }
};

export const searchGetters = {
  getOffers: ({ search }) => search.offers,
  isRequesting: ({ search }) => search.requesting,
  getDirections: ({ search }) => search.directions,
  getSearchRequestId: ({ search }) => search.requestId,
  getOpenedCarrier: ({ search }) => search.openedCarrier
};

export const searchActions = {
  searchOffers: async ({ commit, getters, state }, values) => {
    const token = getters.getToken;

    const api = new SearchApi(token);

    commit("startRequesting");

    await api
      .initSearch(values)
      .then(({ data }) => commit("saveRequestId", data.request_id))
      .catch(() => commit("stopRequesting"));

    await tryGetOffers(api, getters.getSearchRequestId)
      .then(data => {
        commit("saveSearchResults", data);

        commit("stopRequesting");
      })
      .catch(() => {
        // Вывод ошибки
        commit("stopRequesting");
      });
  }
};
