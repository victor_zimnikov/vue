export const appState = {
  version: "0.0.1"
};

export const appMutations = {};

export const appGetters = {
  getVersion: state => state.app.version
};
