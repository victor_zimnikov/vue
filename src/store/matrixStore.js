import {
  countNonEmptyRows,
  replaceRows,
  subtractRows
} from "../helpers/MatrixUtils";

export const matrixState = {
  matrix: [
    [0, 0, 1, 0],
    [1, 0, 1, 1],
    [0, 0, 0, 1],
    [1, 0, 0, 0],
    [0, 1, 0, 0]
  ],
  matrixList: []
};

export const matrixMutations = {
  calculateMatrix: state => {
    const firstMatrix = replaceRows(state.matrix.matrix, 1, 0);
    const secondMatrix = subtractRows(firstMatrix, 0, 3);
    const thirdMatrix = replaceRows(secondMatrix, 4, 1);
    const fourthMatrix = replaceRows(thirdMatrix, 3, 2);
    const fifthMatrix = subtractRows(fourthMatrix, 2, 4, -1);
    const sixthMatrix = subtractRows(fifthMatrix, 3, 4, -1);

    const matrixRank = countNonEmptyRows(sixthMatrix);

    state.matrix = {
      ...state.matrix,

      matrixList: [
        {
          title: "Переставим строки 2 и 1",
          matrix: firstMatrix
        },
        {
          title: "Вычтем из 4-й строки 1-ю строку",
          matrix: secondMatrix
        },
        {
          title: "Переставим строки 5 и 2",
          matrix: thirdMatrix
        },
        {
          title: "Переставим строки 4 и 3",
          matrix: fourthMatrix
        },
        {
          title: "Вычетам из 5-й строки 3-ю, умноженную на -1",
          matrix: fifthMatrix
        },
        {
          title: "Вычетам из 5-й строки 4-ю, умноженную на -1",
          matrix: sixthMatrix
        },
        {
          title: "Ранг матрицы равен",
          matrix: matrixRank
        }
      ]
    };
  }
};

export const matrixActions = {};

export const matrixGetters = {
  getMatrix: ({ matrix }) => matrix.matrix,
  getMatrixList: ({ matrix }) => matrix.matrixList
};
