export const authState = {
  authData: null
};

export const authMutations = {
  authorize: (state, payload) => {
    state.auth = {
      ...state.auth,
      authData: payload
    };
  }
};

export const authGetters = {
  getToken: ({ auth }) => (auth.authData ? auth.authData.etm_auth_key : null)
};
