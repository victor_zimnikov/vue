import Vue from "vue";
import Vuex from "vuex";
import { appGetters, appMutations, appState } from "./appStore";
import { authGetters, authMutations, authState } from "./authStore";
import {
  searchActions,
  searchGetters,
  searchMutations,
  searchState
} from "./searchStore";
import {
  matrixActions,
  matrixGetters,
  matrixMutations,
  matrixState
} from "./matrixStore";

Vue.use(Vuex);

const state = {
  app: appState,
  auth: authState,
  search: searchState,
  matrix: matrixState
};

export const store = new Vuex.Store({
  state,
  mutations: {
    ...appMutations,
    ...authMutations,
    ...searchMutations,
    ...matrixMutations
  },
  getters: {
    ...appGetters,
    ...authGetters,
    ...searchGetters,
    ...matrixGetters
  },
  actions: {
    ...searchActions,
    ...matrixActions
  }
});
